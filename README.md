<table align="center"><tr><td align="center" width="9999">
<img src="https://gitlab.com/oskarvid/huntcomputevm/-/raw/master/.huntVMLogo.png" alt="HUNTcomputeVM" width="450" align="center">

# Ansible script to set up the compute environment for the desensitize workflow

Configuring VM's with style
</td></tr></table>

## Background  
This ansible playbook is for the Elixir Norway HUNT Cloud workflow that desensitizes Covid-19 viral samples by removing unecessary human DNA that has contaminated the sample.  
The playbook does the following:  
* The `basic.yml` play installs byobu, git, nmon, python3-distutils, miniconda, pip3, nextflow
* The `clone-wfs.yml` play clones the workflows and puts them in a directory  
* The `docker.yml play` installs a couple of dependencies and docker, changes the image and volume install directory from `/var/lib/docker` to `/home/ubuntu/docker`, and finally it builds the docker images for the workflows  
* Finally the `main.yml` playbook executes the different plays  

## Dependencies
* Ansible - 2.10.3 or higher
* evandam.conda and community.general from ansible-galaxy  
* Pip3

## Instructions
0. Clone this repository: `git clone https://gitlab.com/oskarvid/huntcomputevm`  
1. Install pip3: `sudo apt install python3-pip`  
2. Install Ansible: `pip3 install --user ansible==2.10.3`  
3. Install Ansible galaxy role: `ansible-galaxy install evandam.conda`  
4. Install Ansible galaxy collection: `ansible-galaxy collection install community.general`  
5. Create the `hosts` file and add an entry for the VM, name the VM `desensitizeVM` so you don't need to edit the Ansible yaml files  
6. Run `ansible-playbook -i hosts main.yml` and marvel at the beauty of automated configuration of virtual machines!  
7. If you run it locally you should use this flag too `--connection=local`  
